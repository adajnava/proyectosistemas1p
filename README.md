# ProyectoSistemas1P
Integrantes: 
Adam Jose Navarrete Mora
David Manuel S�nchez Chinga

Nuestro programa se basa practicamente en el comando de linux wc (a continuaci�n el mensaje de ayuda del comando):
wc [option] FILE
wc, o "word count," muestra en consola el n�mero de saltos de l�nea, palabras, y caracteres del archivo de entrada.
-h: ayuda, muestra este mensaje
-l: muestra el total de saltos de l�nea
-w: muestra el total de palabras
-c: muestra el total de caracteres
Para poder acceder al comando se debe de escribir $ ./cp <comando>? <nombre_archivo>.txt
donde:

<comando> son las palabras claves -h , -l , -w , -c , -b:
-h: ayuda, muestra este mensaje
-l: muestra el total de saltos de l�nea
-w: muestra el total de palabras
-c: muestra el total de caracteres
-b: busca la palabra en el archivo y nos devuelve cuantas hay si no hay retorna 0

<comando>? nos dice que puede existir como no puede existir.

<nombre_archivo> nombre del archivo a buscar 
 
