#include<stdio.h>
#include"header.h"
#include <string.h>

int buscarpalabras(char * nombre,char * palabra);
int contarcaracteres(char * nombre);
int contarpalabras(char * nombre);
int contarsaltodelinea(char * nombre);

int main( int argc, // Number of strings in array argv  
	  char *argv[]    // Array of command-line argument strings  
 	 )    
{  


int carac,salto,palabra,cant;
FILE *archivo; 
FILE *archivo1;
char caracter;

archivo = fopen(argv[2],"r"); 
archivo1 =fopen(argv[1],"r"); 

if(argc == 2) { //si hay dos argumentos ... es posible que sea un --help o que sea la lectura de un archivo sin parametros

	if(strcmp(argv[1],"--help")==0){ //el --help
	    printf("aqui te ayudaremos con los comandos:\n-l: muestra el total de saltos de línea\n-b: le pide por teclado buscar una palabra y le retorna el numero de palabras encontradas\n-w: muestra el total de palabras\n-c: muestra el total de caracteres\n");
		    return 1;  
	} 
	else if (archivo1!=NULL){ // lectura de archivo sin parametros por lo tanto nos devuelve todo excepto la busqueda porque para eso nos debemos de ayudar de un ingreso por teclado.
	   carac =contarcaracteres(argv[1]);
	   salto = contarsaltodelinea(argv[1]);
	   palabra = contarpalabras(argv[1]);
	

	    printf("Numero de caracteres: %d\nNumero de saltos de lineas: %d\nNumero de palabras: %d.\n " ,carac,salto, palabra );
		    return 1;
        }
	else{// sino le ayudamos con una pista

		fputs ("quizas quizo decir --help para mostrar la ayuda\n",stderr);
	    
	}

}

if(argc ==3)

    {  
	
	if (archivo!=NULL){ 

		int c=0;
		if(strcmp(argv[1],"-b")==0)

		{
		    char str[20];
		    printf("Ingrese palabra a buscar: ");
		    scanf("%s",str);
	            cant =buscarpalabras(argv[2],str);
		    printf("cantidad de palabras encontradas es: %d\n",cant);
			c=1;
		    return 1;
		}

				
		if(strcmp(argv[1],"-c")==0)

		{
                    cant = contarcaracteres(argv[2]);
		    printf("cantidad de caracteres es: %d\n",cant);
		    c=1;
		    return 1;
		}

				
		if(strcmp(argv[1],"-h")==0)

		{
		    printf("aqui te ayudaremos con los comandos:\n-l: muestra el total de saltos de línea\n-b: le pide por teclado buscar una palabra y le retorna el numero de palabras encontradas\n-w: muestra el total de palabras\n-c: muestra el total de caracteres\n");
		c=1;
		    return 1;
		}

				
		if(strcmp(argv[1],"-l")==0)
		{ 
	            cant = contarsaltodelinea(argv[2]);
		    printf("cantidad de salto de linea: %d\n",cant);
		c=1;
						
	    		return 1;
    		}


	




	    if(strcmp(argv[1],"-w")==0)
	    {
		cant = contarpalabras(argv[2]);
		printf("total de palabras es: %d\n",cant);
		c=1;
	    return 1;
	    }


		if(c==0){

		fputs ("archivo si existe pero el comando es invalido\n",stderr);
		return 1;
		    
		}}

	else{ 
		fputs ("archivo no encontrado\n",stderr);
		return 1;
	}
	

} 

if (argc < 2|| argc > 3){
	fputs ("El numero de argumentos proporcionados no es el correcto \n",stderr);
		return 1;
}

} 

